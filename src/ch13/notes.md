# Chapter 13

## IO

* It is not necessary, and considered bad style, to use `do` in single-line expressions.
  * Prefer `>>=` in single-line expressions instead of do
