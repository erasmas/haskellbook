data Quad
  = One
  | Two
  | Three
  | Four
  deriving (Eq, Show)

-- According to the equality of a -> b and b ^ a,
-- how many different forms can this take?

-- 4 * 4 = 16
eQuad :: Either Quad Quad
eQuad = undefined

-- 4 * 4 = 16
prodQuad :: (Quad, Quad)
prodQuad = undefined

-- 4 ^ 4 = 256
funcQuad :: Quad -> Quad
funcQuad = undefined

-- 2 * 2 * 2 = 8
prodTBool :: (Bool, Bool, Bool)
prodTBool = undefined

-- 2 * (2 ^ 2) = 64
gTwo :: Bool -> Bool -> Bool
gTwo = undefined

-- 4 ^ (2 * 4) = 65536
-- That type is equivalent to fTwo :: (Bool, Quad) -> Quad
fTwo :: Bool -> Quad -> Quad
fTwo = undefined
