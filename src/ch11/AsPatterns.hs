module AsPatterns where

import           Data.Char

isSubseqOf :: (Eq a) => [a] -> [a] -> Bool
isSubseqOf [] _ = True
isSubseqOf sub s = foldr (\x -> (&&) (x `elem` s)) True sub

split :: (Char -> Bool) -> String -> [String]
split _ [] = []
split p s = x : split p (drop 1 y)
  where (x, y) = span p s

splitOn :: Char -> String -> [String]
splitOn c = split (/= c)

capitalizeWord :: String -> String
capitalizeWord [] = []
capitalizeWord (c : cs) = toUpper c : cs

capitalizeWords :: String -> [(String, String)]
capitalizeWords s =
  foldr (\w acc -> (w, capitalizeWord w) : acc) [] (words s)

trimFirst :: String -> String
trimFirst [] = []
trimFirst s @ (c : cs)
  | c == ' '    = trimFirst cs
  | otherwise   = s

join :: String -> [String] -> String
join _ [] = []
join d (w : ws) = foldl (\acc s -> acc ++ d ++ s) w ws

capitalizeParagraph :: String -> String
capitalizeParagraph s =
  let
    appendDot s' = s' ++ "."
    sentence  = appendDot . capitalizeWord . trimFirst
  in unwords $ map sentence $ splitOn '.' s
