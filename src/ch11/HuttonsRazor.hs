module HuttonsRazor where

data Expr
  = Lit Integer
  | Add Expr
        Expr

eval :: Expr -> Integer
eval (Add a b) = (eval a) + (eval b)
eval (Lit a)   = a

printExpr :: Expr -> String
printExpr (Add a b) = printExpr a ++ " + " ++ printExpr b
printExpr (Lit a)   = show a
