# Chapter 11

## newtype
A newtype is similar to a type synonym in that the representations of the named type and the type
it contains are identical and any distinction between them is gone at compile time.
However, one key contrast between a newtype and a type alias is that you can de ne typeclass
instances for newtypes that differ from the instances for their underlying type.

## Product types

A product type’s cardinality is the product of the cardinalities of its inhabitants. Arith- metically, products are the result of multiplication. Where a sum type was expressing or, a product type expresses and.
