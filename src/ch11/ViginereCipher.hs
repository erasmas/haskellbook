module VigenereCipher where

import Data.Char (chr, ord)

-- Here's another implementation that supports any string:
-- https://github.com/adpoe/vigenere_cipher/blob/master/vigenereCipher.hs

shift :: (Int -> Int -> Int) -> Char -> Char -> Char
shift op offset ch = numToChar $ (charToNum ch) `op` (charToNum offset)
  where
    charToNum ch' = ord ch' - ord 'a'
    numToChar n   = chr $ (n `mod` 26) + ord 'a'

vigenere :: String -> String -> String
vigenere secret = zipWith (shift (+)) (cycle secret) . concat . words

unvigenere :: String -> String -> String
unvigenere secret = zipWith (shift (-)) (cycle secret) . concat . words

main :: IO ()
main = do
  putStr "Enter secret: "
  secret <- getLine
  putStr "Enter phrase to encrypt: "
  phrase <- getLine
  putStrLn $ "Encoded phrase: " ++ vigenere secret phrase
