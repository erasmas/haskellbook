{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

data PugType =
  PugData

data HuskyType a =
  HuskyData

data DogueDeBordeaux doge =
  DogueDeBordeaux doge

myPug = PugData :: PugType

myHusky :: HuskyType a
myHusky = HuskyData

myOtherHusky :: Num a => HuskyType a
myOtherHusky = HuskyData

myDoge :: DogueDeBordeaux Int
myDoge = DogueDeBordeaux 10

badDoge :: DogueDeBordeaux String
badDoge = DogueDeBordeaux "hello"

data Doggies a
  = Husky a
  | Mastiff a
  deriving (Eq, Show)

-- 1. Is Doggies a type constructor or a data constructor?
-- Doggies is a type constructor
-- 2. What is the kind of Doggies?
-- Doggies :: * -> *
-- 3. What is the kind of Doggies String?
-- Doggies String :: *
-- 4. What is the type of Husky 10?
-- Husky 10 :: Num a => Doggies a
-- 5. What is the type of Husky (10 :: Integer)?
-- Husky (10 :: Integer) :: Doggies Integer
-- 6. What is the type of Mastiff "Scooby Doo"?
-- Mastiff "Scooby Doo" :: Doggies [Char]
-- 7. Is DogueDeBordeaux a type constructor or a data constructor?
-- There's a type constructor and a data constructor named as DogueDeBordeaux.
-- 8. What is the type of DogueDeBordeaux?
-- DogueDeBordeaux :: doge -> DogueDeBordeaux doge
-- 9. What is the type of DogueDeBordeaux "doggie!"
-- DogueDeBordeaux "doggie" :: DogueDeBordeaux [Char]
data Price =
  Price Integer
  deriving (Eq, Show)

data Manufacturer
  = Mini
  | Mazda
  | Tata
  deriving (Eq, Show)

data Airline
  = PapuAir
  | CatapultsR'US
  | TakeYourChancesUnited
  deriving (Eq, Show)

data Size =
  Size Integer
  deriving (Eq, Show)

data Vehicle
  = Car Manufacturer
        Price
  | Plane Airline
          Size
  deriving (Eq, Show)

myCar :: Vehicle
myCar = Car Mini (Price 14000)

urCar = Car Mazda (Price 20000)

clownCar = Car Tata (Price 7000)

doge = Plane PapuAir

isCar :: Vehicle -> Bool
isCar (Car _ _) = True
isCar _ = False

isPlane :: Vehicle -> Bool
isPlane (Plane _ _) = True
isPlane _ = False

areCars :: [Vehicle] -> [Bool]
areCars = map isCar

getManu :: Vehicle -> Manufacturer
getManu (Car m _) = m
getManu _ = error "illegal argument"

-- newtype
class TooMany a where
  tooMany :: a -> Bool

instance TooMany Int where
  tooMany n = n > 42

newtype Goats =
  Goats Int
  deriving (Eq, Show, TooMany)

-- Following have to be defined when GeneralizedNewtypeDeriving
-- language pragma is not enabled.
-- instance TooMany Goats where
--   tooMany (Goats n) = tooMany n
-- instance TooMany (Int, String) where
--   tooMany (n, _) = n > 42
newtype Pair =
  Pair (Int, String)
  deriving (Eq, Show)

instance TooMany Pair where
  tooMany (Pair (n, _)) = n > 42

newtype Group =
  Group (Int, Int)
  deriving (Eq, Show)

instance TooMany Group where
  tooMany (Group (m, n)) = (m + n) > 42

instance (Num a, TooMany a) => TooMany (a, a) where
  tooMany (x, y) = tooMany $ x + y

------------------------------------------------------------
-- Records
------------------------------------------------------------
-- data Person =
--   MkPerson String
--            Int
--   deriving (Eq, Show)
data Person = Person
  { name :: String
  , age :: Int
  } deriving (Eq, Show)

------------------------------------------------------------
-- Exercises: How Does Your Garden Grow?
type Gardener = String

------------------------------------------------------------
-- data FlowerType
--   = Gardenia
--   | Daisy
--   | Rose
--   | Lilac
--   deriving (Show)
-- data Garden =
--   Garden Gardener
--          FlowerType
--   deriving (Show)
-- What is the normal form of Garden?
data Garden
  = Gardenia Gardener
  | Daisy Gardener
  | Rose Gardener
  | Lilac Gardener
  deriving (Show)

-- Weekday is a type with five data constructors
data Weekday =
         Monday
       | Tuesday
       | Wednesday
       | Thursday
       | Friday

f :: Weekday -> String
f Friday = "Miller Time"

-- Types defined with the data keyword
-- b) must begin with a capital letter

-- The function g
-- c) delivers the final element of xs
g xs = xs !! (length xs - 1)
