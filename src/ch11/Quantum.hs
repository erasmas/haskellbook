module Quantum where

data Quantum
  = Yes
  | No
  | Both
  deriving (Eq, Show)

convert :: Quantum -> Bool

convert Yes = True
convert No  = True
convert Both = True

-- Other possible implementations:

-- convert Yes = False
-- convert No  = True
-- convert Both = True

-- convert Yes = True
-- convert No  = False
-- convert Both = True

-- convert Yes = True
-- convert No  = True
-- convert Both = False

-- convert Yes = False
-- convert No  = False
-- convert Both = True

-- convert Yes = True
-- convert No  = False
-- convert Both = False

-- convert Yes = False
-- convert No  = True
-- convert Both = False

-- convert Yes = False
-- convert No  = False
-- convert Both = False
