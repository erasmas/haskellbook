```
Prelude> (+1) 2
3
```
Writing expressions like (+1) is called **sectioning** and allows you to pass around partially applied functions.