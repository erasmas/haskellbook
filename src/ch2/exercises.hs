multiplyByPi :: Floating a => a -> a
multiplyByPi x = pi * x

-- Rewrite with where clauses:
-- let x = 3; y = 1000 in x * 3 + y

mult1 = x * 3 + y
    where
        x = 3
        y = 1000

-- let y = 10; x = 10 * 5 + y in x * 5
mult2 = x * 5 where 
    x = 10 * 5 + y
    y = 10

-- let x = 7
--     y = negate x
--     z = y * 10
-- in z / x + y
mult3 = z / x + y where 
    z = y * 10
    y = negate x
    x = 7

-- -----------------------
-- Parenthesization
-- -----------------------

-- 2 + 2 * 3 - 1
-- 2 + (2 * 3) - 1

-- (^) 10 $ 1 + 1
-- (^) 10 $ (1 + 1)

-- 2 ^ 2 * 4 ^ 5 + 1
-- (2 ^ 2) * (4 ^ 5) + 1

-- -----------------------
-- Equivalent expressions
-- -----------------------

-- *Main> 400 - 37
-- 363
-- *Main> (-) 37 400
-- -363

-- *Main> 100 / 3
-- 33.333333333333336
-- *Main> 100 `div` 3
-- 33
-- *Main> 100 `div` 3

-- *Main> 2 * 5 + 18
-- 28
-- *Main> 2 * (5 + 18)
-- 46

-- -----------------------
-- More fun with functions
-- -----------------------
waxOn = x * 5 where
    x = y ^ 2
    y = z + 8
    z = 7

triple x = x * 3

waxOff x = triple x