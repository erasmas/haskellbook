-- Chapter 4
-- Find the mistakes

-- not True && True
-- not (x == 6)
-- ["Merry"] > ["Happy"]
-- [1, 2, 3] ++ "look at me!"

-- Chapter exercises:

awesome = ["Papuchon", "curry", ":)"]
also = ["Quake", "The Simons"]
allAwesome = [awesome, also]

length' :: [a] -> Int
length' xs = length xs

-- Real length function has the following type
-- length :: Foldable t = t a -> Int

-- -------------------------------------------------------
-- What are the results of the following expressions?

-- length [1, 2, 3, 4, 5]
-- 5

-- length [(1, 2), (2, 3), (3, 4)]
-- 3

-- length allAwesome
-- 2

-- length (concat allAwesome)
-- 5
-- -------------------------------------------------------

-- 6 / length [1, 2, 3]       -- does note type check!
-- 6 `div` length [1, 2, 3]

-- length allAwesome == 2              -- True :: Bool
-- length [1, 'a', 3, 'b']             -- does note type check
-- length allAwesome + length awesome  -- 5
-- (8 == 8) && ('b' < 'a')             -- False :: Bool
-- (8 == 8) && 9                       -- does note type check

-- -------------------------------------------------------
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome s = reverse s == s

f :: (a, b) -> (c, d) -> ((b, d), (a, c))
f x y = ((snd x, snd y), (fst x, fst y))

-- -------------------------------------------------------
-- Correcting syntax

-- 1. Here, we want a function that adds 1 to the length of a string
-- argument and returns that result.

x = (+)
f' xs = w `x` 1
    where w = length xs

-- 2. This is supposed to be the identity function, id.

id' :: a -> a
id' x = x

-- 3. When fixed, this function will return 1 from the value (1, 2).

fst' :: (a, b) -> a
fst' (a, b) = a

-- -------------------------------------------------------
-- Match the function names to their types

-- 1. has the type of show
-- c) Show a => a -> String

-- 2. has the type of (==)
-- c) Eq a -> a -> a -> Bool

-- 3. is the type of fst
-- a) (a, b) -> a

-- 4. is the type of (+)
-- d) (+) :: Num a => a -> a -> a