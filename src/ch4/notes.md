
# Definitions

1. `type` declares a type synonym. A type synonym is a new name for an existing type.
2. `data` declares a new data type. For example `data Bool = False | True`.
3. A `tuple` is an ordered grouping of values. The types of the elements of tuples are allowed to vary, so you can have both `(String, String)` or `(Integer, String)`
4. A `typeclass` is a set of operations defined with respect to a polymorphic type.
5. *Data constructors* in Haskell provide a means of creating values that inhabit a given type.

```haskell
type Name = String
-- Cat is a nullary data constructor (constant) and Dog takes an argument
data Pet = Cat | Dog Name
-- Pet is a type constructor
```

6. *Type constructors* are not values and can only be used in type signatures.