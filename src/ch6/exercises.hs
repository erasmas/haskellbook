-- ----------------------------------------------------------------------------
-- Write the Eq instance for the datatype provided
-- ----------------------------------------------------------------------------

data TisAnInteger =
    TisAn Integer

instance Eq TisAnInteger where 
    (==) (TisAn i) (TisAn i') = i == i'

data TwoIntegers =
    Two Integer Integer

instance Eq TwoIntegers where 
    (==) (Two a b) (Two a' b') = a == a' && b == b'

data StringOrInt =
    TisAnInt Int
  | TisAString String

instance Eq StringOrInt where 
    (==) (TisAnInt i) (TisAnInt i') = i == i'
    (==) (TisAString s) (TisAString s') = s == s'

data Pair a =
    Pair a a

instance Eq a => Eq (Pair a) where 
    (==) (Pair a b) (Pair a' b') = a == a' && b == b'

data Tuple a b =
    Tuple a b

instance (Eq a, Eq b) => Eq (Tuple a b) where 
    (==) (Tuple a b) (Tuple a' b') = a == a' && b == b'

data Which a =
    ThisOne a
  | ThatOne a

instance (Eq a) => Eq (Which a) where 
    (==) (ThisOne a) (ThisOne a') = a == a'
    (==) (ThatOne a) (ThatOne a') = a == a'
    (==) _ _ = False

data EitherOr a b =
    Hello a
  | Goodbye b

instance (Eq a, Eq b) => Eq (EitherOr a b) where 
    (==) (Hello a) (Hello a') = a == a'
    (==) (Goodbye b) (Goodbye b') = b == b'
    (==) _ _ = False

-- ----------------------------------------------------------------------------
-- Tuple Experiment
-- ----------------------------------------------------------------------------

-- divMod returns a tuple representing the results of applying `div` and `mod`
-- divMod 5 2 == (2, 1)

-- quotRem returns a tuple representing the results of applying `quot` and `rem`
-- divMod 5 2 == (2, 1)

-- quot is integer division truncated toward zero
-- integer remainder

-- ----------------------------------------------------------------------------
-- Will They Work?
-- ----------------------------------------------------------------------------

-- 1. max (length [1, 2, 3]) (length [8, 9, 10, 11, 12]) 
-- ✔️

-- 2. compare (3 * 4) (3 * 5)
-- ✔️

-- 3. compare "Julie" True
-- ❌ (compare expects arguments to be of the same type that implements Ord)

-- 4. (5 + 3) > (3 + 6)
-- ✔️️

-- ----------------------------------------------------------------------------
-- Chapter Exercises
-- ----------------------------------------------------------------------------
-- 1. The Eq class
-- a) includes all types in Haskell
-- b) is the same as the Ord class
-- c) makes equality tests possible ️️️️️✔️
-- d) only includes numeric types

-- 2. The typeclass Ord
-- a) allows any two values to be compared ✔️
-- b) is a subclass of Eq ✔️
-- c) is a superclass of Eq
-- d) has no instance for Bool

-- 3. Suppose the typeclass Ord has an operator >. What is the type of >?
-- a) Ord a => a -> a -> Bool ✔️
-- b) Ord a => Int -> Bool
-- c) Ord a => a -> Char
-- d) Ord a => Char -> [Char]

-- 4. In x = divMod 16 12
-- a) the type of x is Integer
-- b) the value of x is undecidable
-- c) the type of x is a tuple ✔️
-- d) x is equal to 12 / 16

-- 5. The typeclass Integral includes
-- a) Int and Integer numbers ✔️
-- b) integral, real, and fractional numbers
-- c) Schrodinger’s cat
-- d) only positive numbers

-- ----------------------------------------------------------------------------
-- Does the following code typecheck? If not, why not?
-- ----------------------------------------------------------------------------

-- data Person = Person Bool
-- printPerson :: Person -> IO ()
-- printPerson person = putStrLn (show person)

-- Person has no instance of Show

data Mood = Blah | Woot deriving Show

instance Eq Mood where
    (==) Blah Blah = True
    (==) Woot Woot = True
    (==) _ _       = False

settleDown :: Mood -> Mood
settleDown x = if x == Woot
    then Blah
    else x

-- The following code typechecks just fine
type Subject = String
type Verb = String
type Object = String

data Sentence =
    Sentence Subject Verb Object
    deriving (Eq, Show)
    
s1 = Sentence "dogs" "drool"
s2 = Sentence "Julie" "loves" "dogs"

-- ----------------------------------------------------------------------------
-- Which of the following will typecheck?
-- ----------------------------------------------------------------------------
data Rocks = Rocks String deriving (Eq, Show)
data Yeah = Yeah Bool deriving (Eq, Show)
data Papu = Papu Rocks Yeah deriving (Eq, Show)

-- phew = Papu "chases" True                     ❌

truth = Papu (Rocks "chomskydoz") (Yeah True) 

equalityForall :: Papu -> Papu -> Bool
equalityForall p p' = p == p'

-- comparePapus :: Papu -> Papu -> Bool          ❌
-- comparePapus p p' = p > p'

-- ----------------------------------------------------------------------------
-- Match the types
-- ----------------------------------------------------------------------------

-- 1. No instance of Num a
-- i :: a
-- i = 1

-- 2. 
-- a. ️️✔️ ️️️️️ ️
-- b. x

-- 3️
-- a. ️️✔️ ️️️️️ ️
-- b. ✔️ ️️️️️ 

-- 5
-- a) ️️✔️ ️️️️️ ️
-- b) ️️✔️ ️️️️️ ️

-- 6
-- a. ️️✔️ ️️️️️ ️
-- b. ️️✔️ ️️️️️ ️

-- 7
-- a. ️️✔️ ️️️️️ ️
-- b. x

-- 8
-- a. ️️✔️ ️️️️️ ️
-- b. x

-- 9
-- a. ️️✔️ ️️️️️ ️
-- b. ️️✔️ ️️️️️ ️

-- 10
-- a. ️️✔️ ️️️️️ ️
-- b. ️️✔️ ️️️️️ ️

-- 11
-- a. ️️✔️ ️️️️️ ️
-- b. x

-- ----------------------------------------------------------------------------
-- Type-Kwon-Do Two: Electric Typealoo
-- ----------------------------------------------------------------------------
chk :: Eq b => (a -> b) -> a -> b -> Bool
chk aToB a b = (aToB a) == b

arith :: Num b => (a -> b) -> Integer -> a -> b
arith aToB x a = (aToB a) * fromInteger x