module PoemLines where 

split :: String -> Char -> [String]
split [] _ = []
split (s : xs) c
  | s == c = split xs c
split s c  = a : split b c
    where
    a = takeWhile (/= c) s
    b = dropWhile (/= c) s

myWords :: String -> [String]
myWords s = split s ' '

-- Implement this
myLines :: String -> [String]
myLines s = split s '\n'

firstSen = "Tyger Tyger, burning bright\n"
secondSen = "In the forests of the night\n"
thirdSen = "What immortal hand or eye\n"
fourthSen = "Could frame thy fearful\
\ symmetry?"
sentences = firstSen ++ secondSen
    ++ thirdSen ++ fourthSen

-- What we want 'myLines sentences'
-- to equal
shouldEqual =
    [ "Tyger Tyger, burning bright"
    , "In the forests of the night"
    , "What immortal hand or eye"
    , "Could frame thy fearful symmetry?"
    ]
    -- The main function here is a small test
    -- to ensure you've written your function
    -- correctly.
main :: IO ()
main =
    print $
    "Are they equal? "
    ++ show (myLines sentences == shouldEqual)
