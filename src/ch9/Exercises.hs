module Exercises where

import           Data.Char

filterLowerChars :: String -> String
filterLowerChars = filter isUpper

capitalizeFirst :: String -> String
capitalizeFirst ""       = ""
capitalizeFirst (c : cs) = [toUpper c] ++ cs

capitalizeAll :: String -> String
capitalizeAll ""       = ""
capitalizeAll (c : cs) = [toUpper c] ++ (capitalizeAll cs)

firstCharCapitalized :: String -> Char
firstCharCapitalized = head . capitalizeAll

-- Writing your own standard functions

myOr :: [Bool] -> Bool
myOr [] = False
myOr (x : xs) = x || myOr xs

myAny :: (a -> Bool) -> [a] -> Bool
myAny _ [] = False
myAny f (x : xs) = f x || myAny f xs

myElem :: Eq a => a -> [a] -> Bool
-- myElem x (y : []) = x == y
-- myElem x (y : ys) = myElem x [y] || myElem x ys
myElem x = any (\y -> y == x)

myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x : xs) = myReverse xs ++ [x]

squish :: [[a]] -> [a]
squish [] = []
squish (x : xs) = x ++ squish xs

squishMap :: (a -> [b]) -> [a] -> [b]
squishMap f xs = squish $ map f xs

squishAgain :: [[a]] -> [a]
squishAgain = squishMap id

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy cmp (x : xs) = go cmp xs x
  where go _ [] acc = acc
        go cmp (x : xs) acc
          | acc `cmp` x == GT = go cmp xs acc
          | otherwise = go cmp xs x

myMinimumBy :: (a -> a -> Ordering) -> [a] -> a
myMinimumBy cmp (x : xs) = go cmp xs x
  where go _ [] acc = acc
        go cmp (x : xs) acc
          | acc `cmp` x == LT = go cmp xs acc
          | otherwise = go cmp xs x

myMaximum :: (Ord a) => [a] -> a
myMaximum = myMaximumBy compare

myMinimum :: (Ord a) => [a] -> a
myMinimum = myMinimumBy compare
