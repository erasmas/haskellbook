import           PoemLines (myWords)

eftBool :: Bool -> Bool -> [Bool]
eftBool = enumFromTo

eftOrd :: Ordering -> Ordering -> [Ordering]
eftOrd = enumFromTo

eftInt :: Int -> Int -> [Int]
eftInt = enumFromTo

eftChar :: Char -> Char -> [Char]
eftChar = enumFromTo

mySqr = [x ^ 2 | x <- [1 .. 5]]

myCube = [y ^ 3 | y <- [1 .. 5]]

squaresAndCubes = [(s, c) | s <- mySqr, c <- myCube, s < 50, c < 50]

-- --------------------------------------------------------
-- Exercises: Filtering
-- --------------------------------------------------------
multipleOfThree x = (rem x 3) == 0

numberOfmultiplesOfThree = length . filter multipleOfThree $ [1 .. 30]

isArticle :: String -> Bool
isArticle s =
  case s of
    "a"       -> True
    "an"      -> True
    "the"     -> True
    otherwise -> False

myFilter :: String -> [String]
myFilter s =
  let words = myWords s
  in filter (\s -> not (isArticle s)) words

-- --------------------------------------------------------
-- Exercises: Zipping
-- --------------------------------------------------------
zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' f [] []         = []
zipWith' f _ []          = []
zipWith' f [] _          = []
zipWith' f (a:as) (b:bs) = (f a b) : zipWith' f as bs

zip' :: [a] -> [b] -> [(a, b)]
zip' (a:as) (b:bs) =
  let f = (,)
  in (f a b) : zipWith' as bs
