dividedBy :: Integral a => a -> a -> (a, a)
dividedBy num denom = go num denom 0 where
    go n d count
      | n < d = (count, n)
      | otherwise =
        go (n - d) d (count + 1)
        
cattyConny :: String -> String -> String
cattyConny x y = x ++ " mrow " ++ y
        
-- fill in the types
flippy :: String -> String -> String
flippy = flip cattyConny

appedCatty :: String -> String
appedCatty = cattyConny "woops"

frappe :: String -> String
frappe = flippy "haha"

summAll :: (Eq a, Num a) => a -> a
summAll n = go n 0 where
    go n result
      | n == 0    = result
      | otherwise = go (n - 1) (result + n)