module InteticsTest where

import Data.List
import Test.Hspec

palindrome :: Integer -> Bool
palindrome n = s == reverse s
    where s = show n

palindromes :: [Integer] -> [Integer]
palindromes range = [p | x <- range, y <- range, let p = x * y, palindrome p]

biggestPalindrome :: [Integer] -> Integer
biggestPalindrome range = maximum $ palindromes range

main :: IO ()
main = hspec $ do
    describe "intetics test" $ do
        it "the biggest palindrome made from the product of two 3-digit numbers" $ do
            biggestPalindrome [100 .. 999] `shouldBe` 906609
