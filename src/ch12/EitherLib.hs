module EitherLib where

catMaybes :: [Maybe a] -> [a]
catMaybes [] = []
catMaybes (Just x:xs) = x : catMaybes xs
catMaybes (Nothing:xs) = catMaybes xs

left' :: Either a b -> Maybe a
left' (Left a)  = Just a
left' (Right _) = Nothing

lefts'' :: [Either a b] -> [Maybe a]
lefts'' = foldr (\a acc -> left' a : acc) []

lefts' :: [Either a b] -> [a]
lefts' = catMaybes . lefts''

right' :: Either a b -> Maybe b
right' (Left _)  = Nothing
right' (Right b) = Just b

rights'' :: [Either a b] -> [Maybe b]
rights'' = foldr (\a acc -> right' a : acc) []

rights' :: [Either a b] -> [b]
rights' = catMaybes . rights''

partitionEithers' :: [Either a b] -> ([a], [b])
partitionEithers' xs = (lefts' xs, rights' xs)

eitherMaybe' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe' _ (Left _) = Nothing
eitherMaybe' f (Right b) = Just $ f b

either' :: (a -> c) -> (b -> c) -> Either a b -> c
either' f _ (Left a) = f a
either' _ f (Right b) = f b

eitherMaybe'' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe'' f = either' (const Nothing) (Just . f)
