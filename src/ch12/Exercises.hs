module Exercises where

import Data.Maybe (fromMaybe)

notThe :: String -> Maybe String
notThe s =
  case s of
    "the" -> Nothing
    _ -> Just s

nothA :: Maybe String -> String
nothA = fromMaybe "a"

replaceThe :: String -> String
replaceThe s = concatMap (nothA . notThe) $ words s

vowels :: String
vowels = "aeiou"

isVowel :: Char -> Bool
isVowel c = c `elem` vowels

-- >>> countTheBeforeVowel "the cow"
-- 0
-- >>> countTheBeforeVowel "the evil cow"
-- 1
countTheBeforeVowel :: String -> Integer
countTheBeforeVowel [] = 0
countTheBeforeVowel s = go 0 (words s)
  where
    go cnt [] = cnt
    go cnt ("the":((c:_):rest))
      | isVowel c = go (cnt + 1) rest
      | otherwise = go cnt rest
    go cnt (_:xs) = go cnt xs

countVowels :: String -> Integer
countVowels = toInteger . length . filter isVowel

newtype Word' =
  Word' String
  deriving (Eq, Show)

mkWord :: String -> Maybe Word'
mkWord s =
  let vowels' = countVowels s
      consonants = (toInteger . length) s - vowels'
  in if vowels' > consonants
       then Nothing
       else Just $ Word' s

-- As natural as any
-- competitive bodybuilder
data Nat =
  Zero
  | Succ Nat
  deriving (Eq, Show)

-- >>> natToInteger Zero
-- 0
-- >>> natToInteger (Succ Zero)
-- 1
-- >>> natToInteger (Succ (Succ Zero)) -- 2
natToInteger :: Nat -> Integer
natToInteger (Succ n) = 1 + natToInteger n
natToInteger Zero     = 0

-- >>> integerToNat 0
-- Just Zero
-- >>> integerToNat 1
-- Just (Succ Zero)
-- >>> integerToNat 2
-- Just (Succ (Succ Zero))
-- >>> integerToNat (-1)
-- Nothing

integerToNat' :: Integer -> Nat
integerToNat' 0 = Zero
integerToNat' n = Succ $ integerToNat' $ pred n

integerToNat :: Integer -> Maybe Nat
integerToNat 0 = Just Zero
integerToNat n = Just $ integerToNat' n
