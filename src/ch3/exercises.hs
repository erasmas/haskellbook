----------------------------
-- Exercises: Scope
----------------------------

-- Is y in scope for z?
-- Prelude> let x = 5
-- Prelude> let y = 7
-- Prelude> let z = x * y

-- Answer: yes

-- Is h in scope for g?
-- Prelude> let f = 3
-- Prelude> let g = 6 * f + h

-- Answer: no

-- Is everything we need to execute area in scope?
-- area d = pi * (r * r)
-- r = d / 2

-- Answer: no

area d = pi * (r * r)
    where r = d / 2

-- Answer: Now r and d are in scope for area

----------------------------
-- Exercises: Syntax Errors
----------------------------

-- ++ [1, 2, 3] [4, 5, 6]
-- (++) [1, 2, 3] [4, 5, 6]

-- '<3' ++ ' Haskell'
-- "<3" ++ " Haskell"

-- concat ["<3", " Haskell"]
-- ✔️

----------------------------
-- Chapter Exercises
----------------------------

-- 1.

-- concat [[1, 2, 3], [4, 5, 6]]
-- ✔️

-- ++ [1, 2, 3] [4, 5, 6]
-- [1, 2, 3] ++ [4, 5, 6] or (++) [1, 2, 3] [4, 5, 6]

-- (++) "hello" " world"
-- ✔️

-- ["hello" ++ " world]
-- ["hello" ++ " world"]

-- 4 !! "hello"
-- "hello" !! 4

-- (!!) "hello" 4
-- ✔️

-- take "4 lovely"
-- take 4 "lovely"

-- take 3 "awesome"
-- ✔️

-- 2.

-- concat [[1 * 6], [2 * 6], [3 * 6]]
-- [6,12,18]

-- "rain" ++ drop 2 "elbow"
-- "rainbow"

-- 10 * head [1, 2, 3]
-- 10

-- (take 3 "Julie") ++ (tail "yes")
-- "Jules"

-- concat [tail [1, 2, 3], tail [4, 5, 6], tail [7, 8, 9]]
-- [2,3,5,6,8,9]

----------------------------
-- Building functions
----------------------------

-- Given "Curry is awesome"

-- return "Curry is awesome!":
-- take 16 "Curry is awesome!"

curry1 s = take 16 s

-- return "y":
-- head $ tail "Curry is awesome!"

curry2 s = head $ tail s

-- return "awesome!"
-- drop 9 "Curry is awesome!"

curry3 s = drop 9 s

-- 3.

thirdLetter :: String -> Char
thirdLetter s = s !! 2

-- 4.

letterIndex :: Int -> Char
letterIndex x = "Curry is awesome!" !! x

-- 5.

rvrs s = awesome ++ is ++ curry where
        awesome = drop 9 s
        is      = take 4 $ drop 5 s
        curry   = take 5 s

