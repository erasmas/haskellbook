-- --------------------------------------------------------
-- Exercises: Type Matching
-- --------------------------------------------------------

-- head :: [a] -> a
-- concat :: [[a]] -> [a]
-- not :: Bool -> Bool
-- length :: [a] -> Int
-- (<) :: Ord a => a -> a -> Bool

-- --------------------------------------------------------
-- Exercises: Type Arguments
-- --------------------------------------------------------

-- 1. If the type of f is a -> a -> a -> a, and the type of x is Char then the type of `f x`
-- is `Char -> Char -> Char`

-- 2. If the type of g is a -> b -> c -> b, then the type of g 0 'c' "woot"
-- is Char

-- 3. If the type of h is (Num a, Num b) => a -> b -> b, then the type of h 1.0 2
-- is Num b => b

-- 4. If the type of h is (Num a, Num b) => a -> b -> b, then the type of
-- h 1 (5.5 :: Double) is Double

-- 5. If the type of jackal is (Ord a, Eq b) => a -> b -> a, then the type
-- of `jackal "keyboard" "has the word jackal in it"` is [Char]

-- 6. If the type of jackal is (Ord a, Eq b) => a -> b -> a, then the type
-- of jackal "keyboard" is `Eq b => b -> [Char]`

-- 7. If the type of kessel is (Ord a, Num b) => a -> b -> a, then the
-- type of `kessel 1 2` is `(Num a, Ord a) => a`

-- 8. If the type of kessel is (Ord a, Num b) => a -> b -> a, then the
-- type of `kessel 1 (2 :: Integer)` is `(Num a, Ord a) => a`

-- 9. If the type of kessel is (Ord a, Num b) => a -> b -> a, then the
-- type of `kessel (1 :: Integer) 2` is `Integer`

-- TODO: Question why answers for 8 and 9 are so different?

-- --------------------------------------------------------
-- Exercises: Parametricity
-- --------------------------------------------------------

-- 2. Implement function with `a -> a -> a` signature
-- oneOf :: a -> a -> a
-- oneOf a1 _ = a1
-- oneOf _ a2 = a2

-- 3. Implement a -> b -> b.
-- A function whith such signature can only have one implementation that doesn't
-- change its behaviour for different types of `a` and `b`
weirdoF :: a -> b -> b
weirdoF a b = b

-- --------------------------------------------------------
-- Exercises: Apply Yourself
-- --------------------------------------------------------

-- 1. general function
-- (++) :: [a] -> [a] -> [a]

-- Applied to a String value
-- (++) :: [Char] -> [Char] -> [Char]
myConcat :: [Char] -> [Char]
myConcat x = x ++ " yo"

-- 2. general function
-- (*) :: Num a => a -> a -> a

-- Applied to a value
-- (*) :: Fractional a => a -> a -> a
myMult x = (x / 3) * 5

-- 3. take :: Int -> [a] -> [a]
-- take :: Int -> [Char] -> [Char]
myTake x = take x "hey you"

-- 4. (>) :: Ord a => a -> a -> Bool
-- (>) :: Int a => a -> a -> Bool
myCom x = x > (length [1..10])

-- 5. (<) :: Ord a => a -> a -> Bool
-- (<) :: Char a => a -> a -> Bool
myAlph x = x < 'z'

-- --------------------------------------------------------
-- Chapter Exercises
-- --------------------------------------------------------
-- Multiple choise
-- 1 c
-- 2 a
-- 3 b 
-- 4 c // TODO c or d?

-- --------------------------------------------------------
-- Does it compile?

-- 1. yes
bigNum = (^) 5 $ 10

-- 2. yes
x = print
y = print "wohoo!"
z = x "hello world"

-- 3. no, as b is not a function
-- a = (+)
-- b = 5
-- c = b 10
-- d = c 200

-- 4. no, c is not in scope
-- a = 12 + b
-- b = 10000 * c

-- --------------------------------------------------------
-- Categorize each component of the type signature

-- (c) - concrete type
-- (fp) - fully polymorphic
-- (cp) - contrained polymorphic

-- f :: zed -> Zed -> Blah
--              (c)    (c)

-- f :: Enum b => a -> b -> C
--               (fp) (cp)  (c)

-- f :: f -> g -> C
--     (fp) (fp)  (c)

-- --------------------------------------------------------
-- Write a type signature

functionH :: [t] -> t
functionH (x:_) = x

functionC :: Ord a => a -> a -> Bool
functionC x y = if (x > y) then True else False

functionS :: (a, b) -> b
functionS (x, y) = y

-- --------------------------------------------------------
-- Given a type, write the function

i :: a -> a
i = \x -> x

-- c and c'' are equal functions

c :: a -> b -> a
c = \a _ -> a

c'' :: b -> a -> b
c'' = \b _ -> b

c' :: a -> b -> b
c' = \_ b -> b

r :: [a] -> [a]
r = tail
-- -- r can be init, tail, sortBy, etc ...

co :: (b -> c) -> (a -> b) -> a -> c
co bToC aToB a = bToC $ aToB a

a :: (a -> c) -> a -> a
a _ a = a

a' :: (a -> b) -> a -> b
a' aToB a = aToB a

-- --------------------------------------------------------
-- Type-Kwon-Do

-- 1

f :: Int -> String
f = undefined

g :: String -> Char
g = undefined

h :: Int -> Char
h x =  (g . f) x

-- 2

data A
data B
data C

q :: A -> B
q = undefined

w :: B -> C
w = undefined

e :: A -> C
e a = (w . q) a

-- 3

data X
data Y
data Z

xz :: X -> Z
xz = undefined

yz :: Y -> Z
yz = undefined

xform :: (X, Y) -> (Z, Z)
xform (x, y) = (xz x, yz y)

-- 4

munge :: (x -> y) -> (y -> (w, z)) -> x -> w
munge xy ywz x = let y = xy x
                     (w, _) = ywz y
                 in w

