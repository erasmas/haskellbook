# Chapter 5

## Recomendations

* Declare types (that is, write the type signatures) for your functions. Types can serve as documentation of your program.

## Notable quotes

"`currying` has, unfortunately, nothing to do with food."

## Notes

* `Uncurrying` means un-nesting the functions and replacing the two functions with a tuple of two values (these would be the two values you want to use as arguments). If you uncurry `(+)`, the type changes from `Num a => a -> a -> a` to `Num a => (a, a) -> a` which better fits the description “takes two arguments, returns one result” than curried functions.
* `sectioning` specifically refers to partial application of infix operators.
  ```haskell
  let x = 5
  let y = (2^)
  let z = (^2)
  ```
* A function is `polymorphic` when its type signature has variables that can represent more than one type.
* Polymorphism in Haskell doesn't allow a subclass to override methods of it's superclass. That is, if something has an instance of Num but not an instance of Integral, it can’t implement the methods of the Integral typeclass.
* `Type inference` is an algorithm for determining the types of expressions. Haskell’s type inference is built on an extended version of the Damas-Hindley-Milner type system.
