module DetermineTheType where
    -- determine the most polymorphic type an expression
    -- could have in the following function applications:

    a :: Integer
    a = (* 9) 6

    b :: Num t => (t, [Char])
    b = head [(0,"doge"),(1,"kitteh")]
    
    c :: (Integer, [Char])
    c = head [(0 :: Integer ,"doge"),(1,"kitteh")]

    d :: Bool
    d = if False then True else False

    e :: Int
    e = length [1, 2, 3, 4, 5]

    f :: Bool
    f = (length [1, 2, 3, 4]) > (length "TACOCAT")

    x = 5
    y = x + 5

    w :: Integer
    w = y * 10

    z :: Num t => t -> t
    z y = y * 10

    f' :: Double
    f' = 4 / fromIntegral y

    j = "Julie"
    l = " <3 "
    h = "Haskell"
    f'' :: [Char]
    f'' = j ++ l ++ h
